import {getInputInfo,renderEmployeeList} from '../controller/employee.controller.js';
import { Employee } from '../model/employee.model.js';
import {validator} from "../model/validator.js";

let employeeList = [];

let employeeListJason = localStorage.getItem("employeeList");
if(employeeListJason!=null){
    let takenList = JSON.parse(employeeListJason);
    employeeList = takenList.map((item)=>{
        return new Employee(item.id,item.fullName,item.email,item.password,item.date,item.salary,item.position,item.workingtime);
    });
    renderEmployeeList(employeeList);
}


document.getElementById("btnThemNV").addEventListener("click",addNewEmployee);
document.getElementById("btnCapNhat").addEventListener("click",updateEmployeeInfo);
document.getElementById("btnTimNV").addEventListener("click",employeeGradeFilter);

function inputValidator(employee,employeeList){    
    // Kiểm tra id
    let validId = validator.checkEmptyInput(employee.id,"tbTKNV","Id") && validator.checkIdLength(employee.id) && validator.checkExistingId(employee.id,employeeList,"tbTKNV");
    // Kiểm tra tên
    let validName  =  validator.checkEmptyInput(employee.fullName,"tbTen","Tên") && validator.checkNameFormat(employee.fullName,"tbTen");
    // Kiếm tra email
    let validEmail = validator.checkEmptyInput(employee.email,"tbEmail","Email")  && validator.checkEmailFormat(employee.email,"tbEmail") && validator.checkExistingEmail(employee.email,employeeList,"tbEmail");
    // Kiểm tra mật khẩu
    let validPassword = validator.checkEmptyInput(employee.password,"tbMatKhau","Mật khẩu") && validator.checkPasswordFormat(employee.password,"tbMatKhau");
    // Kiểm tra ngày
    let validDate = validator.checkEmptyInput(employee.date,"tbNgay","Ngày làm") && validator.checkDateFormat(employee.date,"tbNgay");
    // Kiểm tra lương cơ bản
    let validSalary = validator.checkEmptyInput(employee.salary,"tbLuongCB","Lương cơ bản") && validator.checkSalary(employee.salary,"tbLuongCB");
    // Kiểm tra số giờ làm
    let validTime = validator.checkEmptyInput(employee.workingtime,"tbGiolam","Giờ làm") && validator.checkWorkingtime(employee.workingtime,"tbGiolam");
    // Kiểm tra rỗng
    let validPosition = validator.checkEmptyInput(employee.position,"tbChucVu","Chức vụ");
    
    let isValid = validId & validName & validEmail & validPassword & validDate & validSalary & validTime & validPosition;

    return isValid;

}

function edittingValidator(employee,employeeList,edittingIndex){    
    // Kiểm tra tên
    let validName  =  validator.checkEmptyInput(employee.fullName,"tbTen","Tên") && validator.checkNameFormat(employee.fullName,"tbTen");
    // Kiếm tra email
    let validEmail = validator.checkEmptyInput(employee.email,"tbEmail","Email")  && validator.checkEmailFormat(employee.email,"tbEmail") && validator.checkExistingEmailEditting(employee.email,edittingIndex,employeeList,"tbEmail");
    // Kiểm tra mật khẩu
    let validPassword = validator.checkEmptyInput(employee.password,"tbMatKhau","Mật khẩu") && validator.checkPasswordFormat(employee.password,"tbMatKhau");
    // Kiểm tra ngày
    let validDate = validator.checkEmptyInput(employee.date,"tbNgay","Ngày làm") && validator.checkDateFormat(employee.date,"tbNgay");
    // Kiểm tra lương cơ bản
    let validSalary = validator.checkEmptyInput(employee.salary,"tbLuongCB","Lương cơ bản") && validator.checkSalary(employee.salary,"tbLuongCB");
    // Kiểm tra số giờ làm
    let validTime = validator.checkEmptyInput(employee.workingtime,"tbGiolam","Giờ làm") && validator.checkWorkingtime(employee.workingtime,"tbGiolam");
    // Kiểm tra rỗng
    let validPosition = validator.checkEmptyInput(employee.position,"tbChucVu","Chức vụ");
    
    let isValid = validName & validEmail & validPassword & validDate & validSalary & validTime & validPosition;

    return isValid;

}

document.getElementById("btnThem").addEventListener("click",()=>{
    document.getElementById("header-title").innerText = "Log In";
    document.getElementById('tknv').disabled = false;
    let tbspan =document.querySelectorAll('.sp-thongbao');
    tbspan.forEach((span)=>{
        span.style.display = "none"
    });
});

function addNewEmployee() {
    let employee = getInputInfo();
    let isEditting = document.getElementById("header-title").innerText == "Editting";
    if(!isEditting&&inputValidator(employee,employeeList)){
        employeeList.push(employee);
        let employeeListJason = JSON.stringify(employeeList);
        localStorage.setItem("employeeList",employeeListJason);
        renderEmployeeList(employeeList);
    }
}

function deleteEmployee(index){
    employeeList.splice(index,1);
    let employeeListJason = JSON.stringify(employeeList);
    localStorage.setItem("employeeList",employeeListJason);
    renderEmployeeList(employeeList);
}

function editEmployee(index){
    document.getElementById("header-title").innerText = "Editting";
    showEditInfo(index);
    let tbspan =document.querySelectorAll('.sp-thongbao');
    tbspan.forEach((span)=>{
        span.style.display = "none"
    });
}

function showEditInfo(index){
    let employee = employeeList[index];
    document.getElementById('tknv').value=employee.id;
    document.getElementById('name').value=employee.fullName;
    document.getElementById('email').value=employee.email;
    document.getElementById('password').value=employee.password;
    document.getElementById('datepicker').value=employee.date;
    document.getElementById('luongCB').value=employee.salary;
    document.getElementById("chucvu").value=employee.position;
    document.getElementById('gioLam').value=employee.workingtime;
    document.getElementById('tknv').disabled = true;
}

function updateEmployeeInfo(){
    let inputInfo = getInputInfo()
    let edittingId = document.getElementById("tknv").value;
    var edittingIndex = findIndex(edittingId);
    let isEditting = document.getElementById("header-title").innerText == "Editting";
    if(isEditting&&edittingValidator(inputInfo,employeeList,edittingIndex)){
        employeeList.splice(edittingIndex,1,inputInfo);
        let employeeListJason = JSON.stringify(employeeList);
        localStorage.setItem("employeeList",employeeListJason);
        renderEmployeeList(employeeList);
    }
}

function findIndex(id){
    return employeeList.findIndex(item => item.id == id);
}

function employeeGradeFilter(){
    let searchString = document.getElementById("searchName").value.trim().toLowerCase();
    if(searchString!=""){
        let resultArr = employeeList.filter((item)=>{
        return item.grade().toLowerCase() == searchString;
    });
    if(resultArr.length !=0){
        renderEmployeeList(resultArr);
    } else {
        alert("Không tìm thấy nhân viên có xếp loại:" + searchString);
    }
    } else {
        renderEmployeeList(employeeList);
    }
}

window.deleteEmployee = deleteEmployee;
window.editEmployee = editEmployee;



