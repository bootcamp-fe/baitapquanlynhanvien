import { Employee } from "../model/employee.model.js";

export function getInputInfo(){
    let id = document.getElementById('tknv').value;
    let name = document.getElementById('name').value;
    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;
    let date = document.getElementById('datepicker').value;
    let salary = document.getElementById('luongCB').value;
    let position = document.getElementById("chucvu").value;
    let worktime = document.getElementById('gioLam').value;

    return new Employee(id,name,email,password,date,salary,position,worktime);
}

export function renderEmployeeList(employeeList){
    let contentHTML = "";
    for(var i = 0 ; i < employeeList.length ; i++){
        let employee = employeeList[i];
        let renderString = /**HTML*/ `<tr>
        <td>${employee.id}</td>
        <td>${employee.fullName}</td>
        <td>${employee.email}</td>
        <td>${employee.date}</td>
        <td>${employee.position}</td>
        <td>${employee.totalSalary()}</td>
        <td>${employee.grade()}</td>
        <td>
        <button onclick = deleteEmployee(${i}) name ="deleteBtn" class=" btn btn-warning btn-sm"><i class="bi bi-trash3"></i></button>
        <button data-toggle="modal" data-target="#myModal" onclick = editEmployee(${i}) class=" btn btn-primary btn-sm"><i class="bi bi-pencil-square"></i></button>
        </td>
        </tr>`;
        contentHTML += renderString;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

