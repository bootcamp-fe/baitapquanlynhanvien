export function Employee(id,fullName,email,password,date,salary,position,workingtime){
    this.id = id;
    this.fullName = fullName;
    this.email = email;
    this.password = password;
    this.date = date;
    this.salary = salary;
    this.position = position;
    this.workingtime = workingtime;
    this.totalSalary = function(){
        let totalSalary;
        switch( this.position){
            case "Sếp":
                totalSalary = this.salary*3;
                break;
            case "Trưởng phòng":
                totalSalary = this.salary*2;
                break;
            case "Nhân viên":
                totalSalary = this.salary;
                break;
        }
        return totalSalary;
    };
    this.grade = function(){
        if(this.workingtime < 160){
            return "Trung bình";
        } else if(this.workingtime < 176){
            return "Khá";
        } else if(this.workingtime < 192){
            return "Giỏi";
        } else {
            return "Xuất sắc";
        }
    };
}

