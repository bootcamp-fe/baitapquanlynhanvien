export let validator = {
    // Check for empty input
    checkEmptyInput : (inputValue,inputField,massage = "")=>{
        if(inputValue.length == 0){
            document.getElementById(inputField).style.display = "block";
            document.getElementById(inputField).innerText= `${massage} không được để trống`;
            return false;
        } else {
            document.getElementById(inputField).style.display = "none";
            return true;
        }
    },
    // Check ID length
    checkIdLength : function (value){
        if(value.length<4 || value.length>6 && value.length!=0){
            document.getElementById("tbTKNV").style.display = "block";
            document.getElementById("tbTKNV").innerText = "Hãy nhập tài khoản từ 4 - 6 ký số";
            return false
        } else {
            document.getElementById("tbTKNV").style.display = "none";
            return true;
        }
    },
    // Check existing ID 
    checkExistingId : function(inputId,employeeList,inputField){
        for(var i = 0 ; i < employeeList.length; i++){
            let currentId = employeeList[i].id;
            if(inputId == currentId){
                document.getElementById(inputField).style.display = "block";
                document.getElementById(inputField).innerText = "ID Đã tồn tại";
                return false;
            }
        };
        document.getElementById(inputField).style.display = "none";
        return true;
    },
    // Check input Name
    checkNameFormat : function(value,inputField){
        if(!isNaN(value)){
            document.getElementById(inputField).style.display = "block";
            document.getElementById(inputField).innerText = "Tên phải là chữ";
            return false;
        } else {
            document.getElementById(inputField).style.display = "none";
            return true;
        }
        
    },
    // Check email format
    checkEmailFormat : function (email,inputField){
        let validEmail = String(email).toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        if(validEmail){
            document.getElementById(inputField).style.display = "none";
            return true;
        } else {
            document.getElementById(inputField).style.display = "block";
            document.getElementById(inputField).innerText = "Định dạng email không hợp lệ";
            return false;
        }
    },
    checkExistingEmail : function(email,employeeList,inputField){
        for(var i = 0 ; i < employeeList.length ; i++){
            let currentEmail = employeeList[i].email;
            if(email == currentEmail){
                document.getElementById(inputField).style.display = "block";
                document.getElementById(inputField).innerText = "Email đã tồn tại";
                return false;
            }
        };
        document.getElementById(inputField).style.display = "none";
        return true;
    },
    // Check password format
    checkPasswordFormat : function(password,inputField){
        let validPassword = String(password).match(/^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,10}$/);
        if(validPassword){
            document.getElementById(inputField).style.display = "none";
            return true;
        } else {
            document.getElementById(inputField).style.display = "block";
            document.getElementById(inputField).innerText = "Mật khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)";
            return false;
        }
    },
    // Check date format
    checkDateFormat : function(date,inputField){
        let validDate = String(date).match(/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/);
        if(validDate){
            document.getElementById(inputField).style.display = "none";
            return true;
        } else {
            document.getElementById(inputField).style.display = "block";
            document.getElementById(inputField).innerText = "Nhập ngày theo định dạng mm/dd/yyyy";
            return false;
        }
    },
    // Check salary value
    checkSalary : function(salary,inputField){
        if(salary< 1000000 || salary > 20000000 || isNaN(salary)){
            document.getElementById(inputField).style.display = "block";
            document.getElementById(inputField).innerText = "Nhập lương cơ bản từ 1 000 000 đến 20 000 000";
            return false;
        } else {
            document.getElementById(inputField).style.display = "none";
            return true;
        }
    },
    // Check workingtime
    checkWorkingtime : function(workingtime,inputField){
        if(workingtime < 80 || workingtime > 200 || isNaN(workingtime)){
            document.getElementById(inputField).style.display = "block";
            document.getElementById(inputField).innerText = "Số giờ làm trong tháng 80 - 200 giờ";
            return false;
        } else {
            document.getElementById(inputField).style.display = "none";
            return true;
        };
    },
    // Check Existing Email while editting Info
    checkExistingEmailEditting: function(email,currentIndex,employeeList,inputField){
        for(let i = 0 ; i < employeeList.length; i++){
            if(email == employeeList[i].email && i!=currentIndex){
                document.getElementById(inputField).style.display = "block";
                document.getElementById(inputField).innerText = "Email đã được đăng ký";
                return false;
            }
        }
        document.getElementById(inputField).style.display = "none";
        return true;
    }

}